<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Ten plik zawiera konfiguracje: ustawień MySQL-a, prefiksu tabel
 * w bazie danych, tajnych kluczy, używanej lokalizacji WordPressa
 * i ABSPATH. Więćej informacji znajduje się na stronie
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Kodeksu. Ustawienia MySQL-a możesz zdobyć
 * od administratora Twojego serwera.
 *
 * Ten plik jest używany przez skrypt automatycznie tworzący plik
 * wp-config.php podczas instalacji. Nie musisz korzystać z tego
 * skryptu, możesz po prostu skopiować ten plik, nazwać go
 * "wp-config.php" i wprowadzić do niego odpowiednie wartości.
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'rezerwacje');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'root');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', '');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'TR3}1>m+O1%i`9<fAo&*Y{E*>eV!$~#&<*YU<d~>SdTA 1Sz1&vf%`%GGqd2yAOD');
define('SECURE_AUTH_KEY',  '<xe*=nhQ_Ee0=`;j#hdHF]Ht4b,huyZ?^Yf#h`,&x;b<:HPK!XgvL?[^n(bT0sOz');
define('LOGGED_IN_KEY',    '/@Me<P=:qY]BiiEb%)YbHM$o(&Enmm{_5zPOBW<Y`=3<XJvg};#cGzyVrRUF{Cfv');
define('NONCE_KEY',        'km`N1/Xyfp~TX<9M4-FM/?K8{)O9`-jzKb6)%`{F<bP*4^@,Oee=_).4JKWAR!B5');
define('AUTH_SALT',        't54&q){yJ,Pq2pf2<E7CYKu*T%&{_A!CmZ>$)Zbpm<A=1I#O60=MYpxd(N-<WyR.');
define('SECURE_AUTH_SALT', '5Q|1(iL)0D8Lg~X(:JI)OG]BwOsIc3p&8g|M$eX;l21g88)VQ!z8.~qp9BA4nVTA');
define('LOGGED_IN_SALT',   'xV*<S`Y%Ft7dR#kkW5oYTg93gHKjjqR/_5n^-$yG2Y@qyslJ)1ntQaUggyZZ4maU');
define('NONCE_SALT',       ':32<&% qL2b1w2^j1|,Di-Q0=6N{=>N4Wx2sOJipR&oAKWQ9OWk2i+I%Q|kziFmI');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie ostrzeżeń
 * podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG w miejscach pracy nad nimi.
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
