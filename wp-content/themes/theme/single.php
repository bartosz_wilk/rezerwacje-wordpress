<?php get_header(); ?>
<div id="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="single">
		<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
		<div class="post-image" style="background-image: url('<?php echo $url; ?>');"></div>
		<div class="post-desc">
			<div class="meta">
				<?php the_time('j'); ?> <?php the_time('F'); ?> <?php the_time('Y'); ?> - 
				<?php the_category(); ?>, <?php comments_popup_link( 'Brak komentarzy', '1 Komentarz', '% Komentarzy' ); ?>
			</div>
			<h2><?php the_title(); ?></h2>
			<div class="post-content">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
<?php endwhile; ?>
	
<?php else : ?>
	<p><?php _e( 'Niestety nie znaleniono żadnych postów.' ); ?></p>
<?php endif; ?>
</div>
<?php get_footer(); ?>