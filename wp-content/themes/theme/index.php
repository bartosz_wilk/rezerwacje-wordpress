<?php get_header(); ?>
<div id="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="post">
		<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
		<div class="post-image" style="background-image: url('<?php echo $url; ?>');"></div>
		<div class="post-desc">
			<div class="meta">
				<?php the_time('j'); ?> <?php the_time('F'); ?> <?php the_time('Y'); ?> - 
				<?php the_category(); ?>, <?php comments_popup_link( 'Brak komentarzy', '1 Komentarz', '% Komentarzy' ); ?>
			</div>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="post-content">
				<?php the_content(); ?>
				<a href="<?php the_permalink(); ?>" class="btn-more"><i class="fa fa-angle-right"></i> Czytaj więcej</a>
			</div>
		</div>
	</div>
<?php endwhile; ?>
	<div id="pagination">
		<?php
			global $wp_query;

			$big = 999999999; // need an unlikely integer

			echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages,
				'prev_text' => '<',
				'next_text' => '>'
			) );
		?>
	</div>
<?php else : ?>
	<p><?php _e( 'Niestety nie znaleniono żadnych postów.' ); ?></p>
<?php endif; ?>
</div>
<?php get_footer(); ?>