<!DOCTYPE html>
<html lang="pl">
<head <?php language_attributes(); ?>>
	<meta charset="utf-8">
	<title><?php wp_title(); ?></title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,600,400' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
</head>

<body>
	